import {Component, OnInit} from '@angular/core';
import { ContactsService } from '../services/contacts.service';
import { Contact } from '../interfaces/interfaces';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})

export class Tab3Page implements OnInit {

  contacts: Contact[] = [];
  favContacts: Contact[] = [];
  otherContacts: Contact[] = [];

  constructor( private contactsService: ContactsService ) {}

  ngOnInit() {
    this.contactsService.load()
        .subscribe( resp => {
            console.log(resp)
            this.contacts.push.apply(this.contacts, resp);
            this.contactsService.setData(this.contacts);
            this.getFavorites();
        });
  }

  getFavorites() {
    this.favContacts = this.contactsService.getFav(true);
    console.log('Favorites: ', this.favContacts);
    this.otherContacts = this.contactsService.getFav(false);
    console.log('Others: ', this.otherContacts);
  }

}
