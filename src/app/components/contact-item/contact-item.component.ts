import { Component, OnInit, Input } from '@angular/core';
import { Contact } from 'src/app/interfaces/interfaces';

@Component({
  selector: 'app-contact-item',
  templateUrl: './contact-item.component.html',
  styleUrls: ['./contact-item.component.scss'],
})
export class ContactItemComponent implements OnInit {

  @Input() contacts: Contact[];

  constructor() { }

  ngOnInit() {}

}

