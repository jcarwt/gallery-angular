import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { ContactItemComponent } from './contact-item/contact-item.component';
import { ContactComponent } from './contact/contact.component';

@NgModule({
    declarations: [
        ContactItemComponent,
        ContactComponent
    ],
    exports: [
        ContactItemComponent,
        ContactComponent
    ],
    imports: [
        CommonModule,
        IonicModule,
        RouterModule
    ]
})
export class ComponentsModule { }
