import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment.prod';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Contact } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class ContactsService {

  data: any;
  contacts: Contact[] = [];

  constructor( private http: HttpClient) {}

  load(): any {
    if (this.data) {
      return of(this.data);
    } else {
      return this.http.get<Contact>(environment.url);
    }
  }

  setData(data: Contact[]) {
    this.contacts = data;
  }

  getFav(type: boolean) {
    const favoritos = [];
    this.contacts.forEach((datos: Contact) => {
      if (datos.isFavorite === type) {
        favoritos.push(datos);
      }
    });
    return favoritos;
  }

  getContactData(id: string) {
    const contact = this.contacts.find(x => x.id === id);
    return contact;
  }

}
